# ComMENT TO BE app
At the end of the week, excercises 3 will include minimal working application 
for commenting. The application can be uses as self standing or to provide commenting 
to some other web app [click for i.e.](http://lkrv.fri.uni-lj.si/crypto-portal/).

The application developet at the end of this week will be used as an template for 
next excercises (v4).

Remember what we need to enable commenting:

* Commenting form in the template,
* Data base to store the comments (with a nickname) - v4,
* Way to access the database and change or read it - v4,
* In a case of local database, we need to create our own REST API - v4.

V3 will include creating appropriate commenting template, enabling displaying all inputed
comments, JavaScript precheck of the form-input. 
All html templates will be transfromed into pug templates. We will 
create one layout and and one mixin element.
To decorate the page, bootstrap will be used.

